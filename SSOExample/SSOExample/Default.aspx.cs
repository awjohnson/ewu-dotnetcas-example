﻿using DotNetCasClient.Security;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SSOExample
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DotNetCasClient.CasAuthentication.GatewayAuthenticate(true);
        }

        protected void Get_Attributes(object sender, EventArgs e)
        {
            Assertion asserts = new Assertion("attributes");

            Dictionary<string, IList<string>> attrs = asserts.Attributes;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Hello ");
            foreach(KeyValuePair<string, IList<string>> entry in attrs)
            {
                sb.Append(entry.Key + " ");

                foreach(string listEntry in entry.Value)
                {
                    sb.Append(listEntry + " ");
                }
                sb.AppendLine();
            }
            attributesLabel.Text = sb.ToString();
        }
        
    }
}