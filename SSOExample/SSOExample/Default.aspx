﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SSOExample.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:Label runat="server" ID="attributesLabel">Hey</asp:Label>
    <asp:Button runat="server" ID="attributesButton" OnClick="Get_Attributes" Text="Click for attributes" />
    </div>
    </form>
</body>
</html>
