# EWU Single Sign On for .NET Applications


### 1. Download DotNetCasClient package from Nuget Package Manager
   - Using Nuget Package Manager Console, use this command:

             Install-Package DotNetCasClient
    

### 2. Configure the web.config file

###### * Note: Depending on the version of your project and Visual Studio, you may or may not need to add the following yourself.  

- ___casClientConfig___ will go anywhere within <configuration> </configuration>
    
        <casClientConfig
              casServerLoginUrl="https://cas.example.com/cas/login"
              casServerUrlPrefix="https://cas.example.com/cas/"
              serverName="cas.example.com"
              notAuthorizedUrl="~/NotAuthorized.aspx"
              cookiesRequiredUrl="~/CookiesRequired.aspx"
              redirectAfterValidation="true"
              gateway="false"
              renew="false"
              singleSignOut="true"
              ticketTimeTolerance="5000"
              ticketValidatorName="Cas20"
              proxyTicketManager="CacheProxyTicketManager"
              serviceTicketManager="CacheServiceTicketManager"
              gatewayStatusCookieName="CasGatewayStatus"
         />
          
    #####  Enter this information for EWU Single Sign On: ####
- Note: Ignore angle brackets when entering values for serverName
    
            casServerLoginUrl="https://login.ewu.edu/cas/login"
            casServerUrlPrefix="https://login.ewu.edu/cas"
            serverName="https://<YOUR_SERVER_IP_ADDRESS>:<PORT>"


    __notAuthorizedUrl__  - the page in your project that will be redirected to if the person logging in through SSO is not authorized. 

    __cookiesRequiredUrl__ - the page in your project that will be redirected to if the client  has cookies disabled when needed.

    __redirectAfterValidation__ must always be true

    __ticketValidatorName__ - valid entries are EWU are "Cas20" and "Saml11". 

    #####  Use "Cas20" for:      
    - NetID              
    - User name 
        
    ##### Use "Saml11" for:
    - ewuid                        
    - first name
    - last name                             
    - campus email, phone number
    - active directory group membership

    __proxy/serviceTicketManager__ values should stay the same.
    
    The rest are optional, can be removed if do not need.

----------------------------------------------------------------------------------------------------------------------------
####  Add the following within __<system .web>__

- Note : If you do not see <system .web> add it anywhere within <configuration> </configuration>

        <httpModules>
            <add name="DotNetCasClient" type="DotNetCasClient.CasAuthenticationModule, DotNetCasClient" />
        </httpModules>
        <authentication mode="Forms">
            <forms 
                loginUrl="https://login.ewu.edu/cas/login"
                cookieless="UseCookies" 
                timeout="30"
                defaultUrl="~/Default.aspx"  <!-- This can be changed to any page to go after authentication -->
                path="/"
            />
        </authentication>
----------------------------------------------------------------------------------------------------------------------------

#### Add a <configSections> tag at the top within <configuration>

- Add a __<section>__ inside <configSections>, it should look like the following: 

        <configSections>
            <section name="casClientConfig" 
                     type="DotNetCasClient.Configuration.CasClientConfiguration, DotNetCasClient" />
        </configSections>

----------------------------------------------------------------------------------------------------------------------------
#### Within <configuration>, you need to have the following

- If you do not see the following, go ahead and copy/paste this anywhere within configuration

            <system.webserver>
                    <modules>
                        <remove name="DotNetCasClient" />
                        <add name="DotNetCasClient"
                            type="DotNetCasClient.CasAuthenticationModule,DotNetCasClient" />
                    </modules>
            <system.webserver>

            
----------------------------------------------------------------------------------------------------------------------------

### Everything you need for configuration should be entered by this point, and you should be good to go.
 - ###### Note: for this to work, your project needs to be on your server, if you want to test to see if you at least have the SSO implemented into your project, add the following line to the top of the Page_Load method in your Default.aspx.cs (same as defaultUrl in <forms> tage)

        DotNetCasClient.CasAuthentication.GatewayAuthenticate(true);

For more information on DotNetCasClient visit:
    https://github.com/apereo/dotnet-cas-client